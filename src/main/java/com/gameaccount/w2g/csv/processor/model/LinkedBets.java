package com.gameaccount.w2g.csv.processor.model;

import java.util.List;

public class LinkedBets
{
    private final List< BetInfo > linkedBets;

    private LinkedBets( final Builder builder )
    {
        this.linkedBets = builder.linkedBets;
    }

    public List< BetInfo > getLinkedBets( )
    {
        return linkedBets;
    }

    public static final class Builder
    {
        private List< BetInfo > linkedBets;

        public Builder withLinkedBets( final List< BetInfo > linkedBets )
        {
            this.linkedBets = linkedBets;
            return this;
        }

        public LinkedBets build( )
        {
            return new LinkedBets( this );
        }
    }
}
