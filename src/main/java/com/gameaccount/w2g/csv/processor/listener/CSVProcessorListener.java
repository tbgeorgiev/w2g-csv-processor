package com.gameaccount.w2g.csv.processor.listener;

import com.gameaccount.w2g.csv.processor.service.CSVProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class CSVProcessorListener
    implements ApplicationListener<ApplicationReadyEvent>
{
    private final CSVProcessor csvProcessor;

    @Autowired
    public CSVProcessorListener( final CSVProcessor csvProcessor )
    {
        this.csvProcessor = csvProcessor;
    }

    @Override
    public void onApplicationEvent( final ApplicationReadyEvent applicationReadyEvent )
    {
        csvProcessor.initiate( );
    }
}
