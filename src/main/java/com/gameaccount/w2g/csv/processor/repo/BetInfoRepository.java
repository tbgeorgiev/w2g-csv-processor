package com.gameaccount.w2g.csv.processor.repo;

import com.gameaccount.w2g.csv.processor.model.EventInfo;

import java.util.Set;

public interface BetInfoRepository
{
    Set<String> getAllPlayerKeys( );

    String getAllBetsForPlayer( String dbKey );

    void addTaxableEvent( String dbKey, EventInfo eventInfo );

    void addNotifiableEvent( String dbKey, EventInfo eventInfo );

    // For testing only
    void showContent( boolean detailedLog );
}
