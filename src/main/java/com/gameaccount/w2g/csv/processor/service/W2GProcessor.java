package com.gameaccount.w2g.csv.processor.service;

import com.gameaccount.w2g.csv.processor.model.BetInfo;
import com.gameaccount.w2g.csv.processor.model.EventInfo;
import com.gameaccount.w2g.csv.processor.model.LinkedBets;
import com.gameaccount.w2g.csv.processor.model.Player;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static com.gameaccount.w2g.csv.processor.util.W2GUtils.*;

@Service
public class W2GProcessor
{
    private static final Logger LOGGER = LoggerFactory.getLogger( W2GProcessor.class );
    private static final List<Future<?>> FUTURE_LIST = new ArrayList< >( );
    private static final ExecutorService EXECUTOR_SERVICE = Executors.newCachedThreadPool( );

    private final RepositoryService repositoryService;

    @Autowired
    public W2GProcessor( final RepositoryService repositoryService )
    {
        this.repositoryService = repositoryService;
    }


    protected void processAllTaxEvents( )
    {
        final long startTime = System.currentTimeMillis();
        LOGGER.info( "Processing all w2g events" );
        final Set<String> allPlayers = repositoryService.getAllPlayerKeys();
        LOGGER.info( "Number of players to process: {}", allPlayers.size() );

        int processedPlayers = 0;
        for ( final String playerId : allPlayers )
        {
            final List<BetInfo> betListForPlayer = new CopyOnWriteArrayList<>( );
            final String betValuesForPlayer = repositoryService.getAllBetsForPlayer( playerId );

            final String[] betsForPlayer = betValuesForPlayer.split( "#" );
            for ( final String betLine : betsForPlayer )
            {
                final String[] betLineInfo = betLine.split( "\\|" );

                if (betLineInfo.length != 6)
                {
                    continue;
                }

                final BetInfo betInfo = new BetInfo.Builder()
                    .withCustomerId( playerId )
                    .withCombinationId( betLineInfo[0] )
                    .withCouponId( betLineInfo[1] )
                    .withOutcomeId( betLineInfo[2] )
                    .withStatusId( betLineInfo[3] )
                    .withStake( new BigDecimal( betLineInfo[4] ) )
                    .withWin( new BigDecimal( betLineInfo[5] ) )
                    .build();

                betListForPlayer.add( betInfo );
            }

            final Player player = new Player.Builder()
                .withPlayerId( playerId )
                .withBets( betListForPlayer )
                .build();

            final Future<?> future = EXECUTOR_SERVICE.submit( new PlayerProcessor( player ) );
            FUTURE_LIST.add( future );

            ++processedPlayers;
            if ( processedPlayers % 10_000 == 0 )
            {
                LOGGER.info( "Initiated {} tasks.",
                             processedPlayers );
            }
        }

        waitUntilAllTasksFinish( );

        logTime( startTime );

    }

    private void logTime( final long startTime )
    {
        final long totalTime = System.currentTimeMillis() - startTime;
        final long minutes = TimeUnit.MILLISECONDS.toMinutes( totalTime );
        final long seconds = TimeUnit.MILLISECONDS.toSeconds( totalTime ) -
                             TimeUnit.MINUTES.toSeconds( minutes );

        final String elapsedTime = String.format("%02d min, %02d sec",
                      minutes,
                      seconds );

        LOGGER.info( "All tasks finished. Elapsed time: {}",
                     elapsedTime );
    }

    private void waitUntilAllTasksFinish( )
    {
        // block main until all threads are done
        LOGGER.info( "No more tasks to initiate. Waiting for {} tasks to finish..",
                     FUTURE_LIST.size() );

        boolean tasksFinished = false;
        while ( !tasksFinished )
        {
            int counter = 0;
            for ( final Future<?> future : FUTURE_LIST )
            {
                if ( future.isDone() )
                {
                    counter++;
                }

                if ( counter == FUTURE_LIST.size() )
                {
                    tasksFinished = true;
                    break;
                }

            }

            try
            {
                Thread.sleep( 1000 );
            }
            catch ( InterruptedException e )
            {
                LOGGER.warn( e.getMessage(),
                             e );
            }
        }
    }

    private void processTaxForPlayer( final Player player )
    {
        final List<BetInfo> allBets = player.getAllBets();
        final List<LinkedBets> linkedBetsForPlayer = getLinkedBetsForPlayer( allBets );

        for ( final LinkedBets linkedBets : linkedBetsForPlayer )
        {
            processTaxForLinkedBets( player.getPlayerId(),
                                     linkedBets.getLinkedBets() );
        }

    }

    private List<LinkedBets> getLinkedBetsForPlayer( final List<BetInfo> allBetsForPlayer )
    {
        final List<BetInfo> filteredBets = getFilteredBets( allBetsForPlayer );

        // get all outcomeIds for each bet and set the outcomeIdList
        setOutcomeIdList( filteredBets );

        // remove the duplicates
        removeDuplicates( filteredBets );

        // match any linked bets by comparing the outcomeIdList for each bet
        return getAllLinkedBets( filteredBets );
    }

    /**
     *  Generates a list of bets that contains only bets that
     *  are eligible for w2g tax events. In short it returns
     *  a list of bets that are of status 2 (WIN) and 7 (CASH_OUT).
     *  Additionally it filters for bets which have their status changed.
     *  In such cases the last status will be the determining status.
     * @param allBetsForPlayer - The bets to process.
     * @return - A list of bets eligible for w2g tax calculation.
     */
    private List<BetInfo> getFilteredBets( final List<BetInfo> allBetsForPlayer )
    {
        final List<BetInfo> filteredBets = new ArrayList<>( );
        final Map<String, String> betsWithChangedOutcome = getAllBetsWithChangedStatus( allBetsForPlayer );

        for ( final BetInfo betInfo : allBetsForPlayer )
        {
            if ( betsWithChangedOutcome.size() != 0 )
            {
                final String currentBetId = betInfo.getCouponId().concat( betInfo.getCombinationId() );
                final String finalBetStatus = betsWithChangedOutcome.get( currentBetId );

                if ( finalBetStatus != null && ( !finalBetStatus.equals( "2" ) && !finalBetStatus.equals( "7" ) ) )
                {
                    // skip this bet, as it's last status is not taxable
                    LOGGER.warn( "Skipping bet {} because last status is {}",
                                 betInfo,
                                 finalBetStatus );
                    continue;
                }
            }

            if (betInfo.getStatusId().equals( "2" ) || betInfo.getStatusId().equals( "7" ))
            {
                filteredBets.add( betInfo );
            }
        }

        return filteredBets;
    }

    private Map<String, String> getAllBetsWithChangedStatus( final List<BetInfo> allBetsForPlayer )
    {
        final Map<String, String> betsWithChangedOutcome = new HashMap<>( );
        final Set<String> checkedBets = new HashSet<>( );

        for ( final BetInfo currentBet : allBetsForPlayer )
        {
            final String currentBetId = currentBet.getCouponId().concat( currentBet.getCombinationId( ) );

            if ( !checkedBets.contains( currentBetId ) )
            {
                for ( final BetInfo nextBet : allBetsForPlayer )
                {
                    final String nextBetId = nextBet.getCouponId().concat( nextBet.getCombinationId() );
                    if ( currentBetId.equals( nextBetId ) &&
                        !currentBet.getStatusId().equals( nextBet.getStatusId( ) ) )
                    {
                        // sets the last status for this bet
                        betsWithChangedOutcome.put( nextBetId, nextBet.getStatusId() );
                        LOGGER.warn( "Bet {} has changed status from {} to {}",
                                     currentBet,
                                     currentBet.getStatusId(),
                                     nextBet.getStatusId() );
                    }
                }
            }

            checkedBets.add( currentBetId );
        }
        return betsWithChangedOutcome;
    }

    /**
     *  Goes over all bets and sets the list of outcomeId's in the
     *  first bet. All other bets should be treated as duplicates.
     * @param filteredBets - The list of bets to process.
     */
    private void setOutcomeIdList( final List<BetInfo> filteredBets )
    {
        for ( int currentIndex = 0; currentIndex < filteredBets.size(); currentIndex++ )
        {
            final Set<String> outcomeIdList = new HashSet<>( );
            outcomeIdList.add( filteredBets.get( currentIndex ).getOutcomeId() );

            final String currentCouponId = filteredBets.get( currentIndex ).getCouponId();
            final String currentCombinationId = filteredBets.get( currentIndex ).getCombinationId();

            for ( int nextIndex = currentIndex + 1; nextIndex < filteredBets.size(); nextIndex++ )
            {
                final String nextCouponId = filteredBets.get( nextIndex ).getCouponId();
                final String nextCombinationId = filteredBets.get( nextIndex ).getCombinationId();

                if ( currentCouponId.equals( nextCouponId ) &&
                    currentCombinationId.equals( nextCombinationId ))
                {
                    outcomeIdList.add( filteredBets.get( nextIndex ).getOutcomeId() );
                }
            }

            filteredBets.get( currentIndex ).setOutcomeIdList( outcomeIdList );

        }
    }

    /**
     * Removes all duplicates. A duplicate is a bet with the
     * same coupon Id, same combination Id and has an empty
     * list of outcome Id's.
     * @param filteredBets - The bets to process.
     */
    private void removeDuplicates( final List<BetInfo> filteredBets )
    {
        for ( int i = 0; i < filteredBets.size( ); i++ )
        {
            if ( i + 1 == filteredBets.size( ) )
            {
                break;
            }

            if ( filteredBets.get( i ).getCouponId( ).equals(
                 filteredBets.get( i + 1 ).getCouponId( ) ) &&
                 filteredBets.get( i ).getCombinationId( ).equals(
                 filteredBets.get( i + 1 ).getCombinationId( ) ) &&
                filteredBets.get( i + 1 ).getOutcomeIdList() == null )
            {
                // Important: always remove the NEXT entry, as the previous
                // one holds the list of outcomeIds
                filteredBets.remove( i + 1 );
                i--;
            }
        }
    }

    private List<LinkedBets> getAllLinkedBets( final List<BetInfo> filteredBets )
    {
        final Set<String> processedBets = new HashSet<>( );
        final List<LinkedBets> listOfLinkedBets = new ArrayList<>( );

        for ( final BetInfo currentBet : filteredBets )
        {
            final List<BetInfo> linkedBets = new ArrayList<>( );
            linkedBets.add( currentBet );

            final String currentBetId = currentBet.getCouponId().concat( currentBet.getCombinationId() );

            if ( processedBets.contains( currentBetId ) )
            {
                continue;
            }

            for ( final BetInfo nextBet : filteredBets )
            {
                final String nextBetId = nextBet.getCouponId().concat( nextBet.getCombinationId() );

                if ( currentBetId.equals( nextBetId ))
                {
                    continue;
                }

                if ( currentBet.getOutcomeIdList().size() == nextBet.getOutcomeIdList().size() &&
                    currentBet.getOutcomeIdList().containsAll( nextBet.getOutcomeIdList() ) )
                {
                    linkedBets.add( nextBet );
                    processedBets.add( nextBetId );
                }
            }

            listOfLinkedBets.add( new LinkedBets.Builder().withLinkedBets( linkedBets ).build( ) );
            processedBets.add( currentBetId );

        }

        return listOfLinkedBets;
    }

    private void processTaxForLinkedBets( final String playerId,
                                          final List<BetInfo> linkedBets )
    {
        final BigDecimal totalStake = getTotalStakes( linkedBets );
        final BigDecimal totalWin = getTotalWins( linkedBets );

        // If a win is >= $5000
        if ( winExceedsRegulatorThreshold( totalWin ) )
        {
            // If quotient is >= 300
            if ( winQuotientExceedsThreshold( totalStake,
                                              totalWin ) )
            {
                addTaxableEvent( playerId,
                                 linkedBets,
                                 totalStake,
                                 totalWin );
            }
        }
        else
        {
            if ( winExceedsNotifiableThreshold( totalStake,
                                                totalWin ) )
            {
                addNotifiableEvent( playerId,
                                    linkedBets,
                                    totalStake,
                                    totalWin );
            }
        }

    }

    private void addNotifiableEvent( final String dbKey,
                                     final List<BetInfo> linkedBets,
                                     final BigDecimal totalStake,
                                     final BigDecimal totalWin )
    {
        LOGGER.debug( "Adding notifiable event with dbKey {}", dbKey );

        final EventInfo eventInfo = createEventInfo( linkedBets,
                                               totalStake,
                                               totalWin,
                                               getQuotient( totalStake,
                                                            totalWin ),
                                               BigDecimal.ZERO );

        repositoryService.addNotifiableEvent( dbKey,
                                              eventInfo );
    }

    private void addTaxableEvent( final String dbKey,
                                  final List<BetInfo> linkedBets,
                                  final BigDecimal totalStake,
                                  final BigDecimal totalWin )
    {
        LOGGER.debug( "Adding taxable event with dbKey {}", dbKey );

        final EventInfo eventInfo = createEventInfo( linkedBets,
                                               totalStake,
                                               totalWin,
                                               getQuotient( totalStake,
                                                            totalWin ),
                                               calculateTaxAmount( totalWin ));

        repositoryService.addTaxableEvent( dbKey,
                                           eventInfo );
    }

    private EventInfo createEventInfo( final List<BetInfo> linkedBets,
                                       final BigDecimal totalStake,
                                       final BigDecimal totalWin,
                                       final BigDecimal quotient,
                                       final BigDecimal federalTaxAmount )
    {
        return new EventInfo.Builder()
            .withPlayerId( linkedBets.get( 0 ).getCustomerId() )
            .withBetIdList( linkedBets.stream().map( BetInfo::getCombinationId ).collect( Collectors.toList( ) ) )
            .withOutcomeIdList( linkedBets.get( 0 ).getOutcomeIdList() )
            .withTotalBetAmount( totalStake )
            .withTotalWin( totalWin )
            .withQuotient( quotient )
            .withTaxableAmount( totalWin )
            .withFederalTaxAmount( federalTaxAmount )
            .build();
    }

    private final class PlayerProcessor implements Runnable
    {
        private final Player player;

        private PlayerProcessor( final Player player )
        {
            this.player = player;
        }

        @Override
        public void run( )
        {
            processTaxForPlayer( player );
        }
    }

}
