package com.gameaccount.w2g.csv.processor.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.math.BigDecimal;

@Configuration
@ConfigurationProperties ( prefix = "w2g" )
public class W2GConfig
{
    private BigDecimal federalTaxThresholdAmount;
    private BigDecimal notifiableThresholdAmount;
    private BigDecimal federalTaxOddsThreshold;
    private BigDecimal federalTaxPercentage;
    private BigDecimal stateTaxPercentage;

    public BigDecimal getFederalTaxThresholdAmount( )
    {
        return federalTaxThresholdAmount;
    }

    public void setFederalTaxThresholdAmount( final BigDecimal federalTaxThresholdAmount )
    {
        this.federalTaxThresholdAmount = federalTaxThresholdAmount;
    }

    public BigDecimal getNotifiableThresholdAmount( )
    {
        return notifiableThresholdAmount;
    }

    public void setNotifiableThresholdAmount( final BigDecimal notifiableThresholdAmount )
    {
        this.notifiableThresholdAmount = notifiableThresholdAmount;
    }

    public BigDecimal getFederalTaxOddsThreshold( )
    {
        return federalTaxOddsThreshold;
    }

    public void setFederalTaxOddsThreshold( final BigDecimal federalTaxOddsThreshold )
    {
        this.federalTaxOddsThreshold = federalTaxOddsThreshold;
    }

    public BigDecimal getFederalTaxPercentage( )
    {
        return federalTaxPercentage;
    }

    public void setFederalTaxPercentage( final BigDecimal federalTaxPercentage )
    {
        this.federalTaxPercentage = federalTaxPercentage;
    }

    public BigDecimal getStateTaxPercentage( )
    {
        return stateTaxPercentage;
    }

    public void setStateTaxPercentage( final BigDecimal stateTaxPercentage )
    {
        this.stateTaxPercentage = stateTaxPercentage;
    }

}
