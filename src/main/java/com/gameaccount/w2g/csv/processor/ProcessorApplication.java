package com.gameaccount.w2g.csv.processor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProcessorApplication
{
	public static void main(String[] args)
	{
		SpringApplication.run(ProcessorApplication.class, args);
	}

}
