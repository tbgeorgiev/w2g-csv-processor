package com.gameaccount.w2g.csv.processor.repo;

import com.gameaccount.w2g.csv.processor.model.EventInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import redis.clients.jedis.Jedis;

import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;

@Repository ("redisImpl")
public class BetInfoRepositoryRedisImpl implements BetInfoRepository
{
    private static final Logger LOGGER = LoggerFactory.getLogger( BetInfoRepositoryRedisImpl.class );

    // Keep taxable and notifiable events in memory only
    private static final List< EventInfo > TAXABLE_EVENTS = new CopyOnWriteArrayList<>( );
    private static final List< EventInfo > NOTIFIABLE_EVENTS = new CopyOnWriteArrayList<>( );

    private final Jedis jedis;


    @Autowired
    public BetInfoRepositoryRedisImpl( final Jedis jedis )
    {
        this.jedis = jedis;
    }

    @Override
    public String getAllBetsForPlayer( final String dbKey )
    {
        return jedis.get( dbKey );
    }

    @Override
    public void addTaxableEvent( final String dbKey,
                                 final EventInfo eventInfo )
    {
        TAXABLE_EVENTS.add( eventInfo );
    }

    @Override
    public void addNotifiableEvent( final String dbKey,
                                    final EventInfo eventInfo )
    {
        NOTIFIABLE_EVENTS.add( eventInfo );
    }


    @Override
    public Set<String> getAllPlayerKeys( )
    {
        return jedis.keys( "*" );
    }

    @Override
    public void showContent( boolean detailedLog )
    {
        LOGGER.info( "TOTAL TAXABLE EVENTS: {}", TAXABLE_EVENTS.size() );
        if ( detailedLog )
        {
            for ( final EventInfo eventInfo : TAXABLE_EVENTS )
            {
                LOGGER.info( eventInfo.toString() );
            }
        }

        LOGGER.info( "TOTAL NOTIFIABLE EVENTS: {}", NOTIFIABLE_EVENTS.size() );
        if ( detailedLog )
        {
            for ( final EventInfo eventInfo : NOTIFIABLE_EVENTS )
            {
                LOGGER.info( eventInfo.toString() );
            }
        }
    }

    public static List< EventInfo > getTaxableEvents( )
    {
        return TAXABLE_EVENTS;
    }

    public static List< EventInfo > getNotifiableEvents( )
    {
        return NOTIFIABLE_EVENTS;
    }
}
