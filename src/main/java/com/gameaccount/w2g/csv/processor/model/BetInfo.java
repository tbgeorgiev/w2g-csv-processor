package com.gameaccount.w2g.csv.processor.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Set;

public class BetInfo
{
//    private static final long serialVersionUID = -4705506809167368600L;

    private final String couponId;
    private final String combinationId;
    private final String outcomeId;
    private Set< String > outcomeIdList;
    private final String customerId;
    private final BigDecimal stake;
    private final BigDecimal win;
    private final String statusId;

    private BetInfo( final Builder builder )
    {
        this.couponId = builder.couponId;
        this.combinationId = builder.combinationId;
        this.outcomeId = builder.outcomeId;
        this.outcomeIdList = builder.outcomeIdList;
        this.stake = builder.stake;
        this.win = builder.win;
        this.customerId = builder.customerId;
        this.statusId = builder.statusId;
    }

    public String getCouponId( )
    {
        return couponId;
    }

    public String getCombinationId( )
    {
        return combinationId;
    }

    public String getOutcomeId( )
    {
        return outcomeId;
    }

    public Set<String> getOutcomeIdList( )
    {
        return outcomeIdList;
    }

    public String getCustomerId( )
    {
        return customerId;
    }

    public BigDecimal getStake( )
    {
        return stake;
    }

    public BigDecimal getWin( )
    {
        return win;
    }

    public String getStatusId( )
    {
        return statusId;
    }

    public void setOutcomeIdList( final Set<String> outcomeIdList )
    {
        this.outcomeIdList = outcomeIdList;
    }

    @Override
    public String toString( )
    {
        return "BetInfo{" + "couponId='" + couponId + '\'' + ", combinationId='" + combinationId + '\'' + ", outcomeId='" + outcomeId + '\'' + ", customerId='" + customerId + '\''
            + ", stake=" + stake + ", win=" + win + ", statusId='" + statusId + '\'' + '}';
    }

    public static final class Builder
    {
        private String couponId;
        private String combinationId;
        private String outcomeId;
        private Set<String> outcomeIdList;
        private BigDecimal stake;
        private BigDecimal win;
        private String customerId;
        private String statusId;

        public Builder withCouponId( final String couponId )
        {
            this.couponId = couponId;
            return this;
        }

        public Builder withCombinationId( final String combinationId )
        {
            this.combinationId = combinationId;
            return this;
        }

        public Builder withOutcomeId( final String outcomeId )
        {
            this.outcomeId = outcomeId;
            return this;
        }

        public Builder withOutcomeIdList( final Set<String> outcomeIdList )
        {
            this.outcomeIdList = outcomeIdList;
            return this;
        }

        public Builder withStake( final BigDecimal stake )
        {
            this.stake = stake;
            return this;
        }

        public Builder withWin( final BigDecimal win )
        {
            this.win = win;
            return this;
        }

        public Builder withCustomerId( final String customerId )
        {
            this.customerId = customerId;
            return this;
        }

        public Builder withStatusId( final String statusId )
        {
            this.statusId = statusId;
            return this;
        }

        public BetInfo build( )
        {
            return new BetInfo( this );
        }

    }
}
