package com.gameaccount.w2g.csv.processor.util;

import com.gameaccount.w2g.csv.processor.config.W2GConfig;
import com.gameaccount.w2g.csv.processor.model.BetInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

@Component
public class W2GUtils
{
    private static W2GConfig w2GConfig;

    @Autowired
    private W2GConfig w2GConfigAutoWired;

    private W2GUtils( )
    { }

    @PostConstruct
    private void initW2GConfig( )
    {
        w2GConfig = this.w2GConfigAutoWired;
    }

    public static boolean isHeader( String firstParam )
    {
        return !firstParam.matches( "[\\d]+" );
    }

    public static BigDecimal getTotalWins( final List<BetInfo> linkedBets )
    {
        return linkedBets
            .stream( )
            .map( BetInfo::getWin )
            .reduce( BigDecimal.ZERO,
                     BigDecimal::add );
    }

    public static BigDecimal getTotalStakes( final List<BetInfo> linkedBets )
    {
        return linkedBets
            .stream( )
            .map( BetInfo::getStake )
            .reduce( BigDecimal.ZERO,
                     BigDecimal::add );
    }

    public static boolean winExceedsRegulatorThreshold( final BigDecimal totalWin )
    {
        final BigDecimal taxThresholdAmount = w2GConfig.getFederalTaxThresholdAmount( );

        return totalWin.compareTo( taxThresholdAmount ) >= 0;
    }

    public static boolean winExceedsNotifiableThreshold( final BigDecimal totalStake,
                                                         final BigDecimal totalWin )
    {
        final BigDecimal notifiableThresholdAmount = w2GConfig.getNotifiableThresholdAmount( );

        if (totalWin.compareTo( notifiableThresholdAmount ) >= 0)
        {
            return winQuotientExceedsThreshold( totalStake, totalWin );
        }

        return false;
    }

    public static BigDecimal getQuotient( final BigDecimal totalStake,
                                          final BigDecimal totalWin )
    {
        return totalWin.divide( totalStake,
                                2,
                                RoundingMode.FLOOR );
    }

    public static boolean winQuotientExceedsThreshold( final BigDecimal totalStake,
                                                       final BigDecimal totalWin )
    {
        final BigDecimal quotient = getQuotient( totalStake,
                                                 totalWin );
        return quotient.compareTo( w2GConfig.getFederalTaxOddsThreshold( ) ) >= 0;
    }

    public static BigDecimal calculateTaxAmount( final BigDecimal win )
    {
        final BigDecimal federalTaxPercentage = w2GConfig.getFederalTaxPercentage( );

        return win
            .multiply( federalTaxPercentage )
            .divide( new BigDecimal( 100 ),
                     2,
                     RoundingMode.FLOOR );
    }
}
