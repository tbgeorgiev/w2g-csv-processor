package com.gameaccount.w2g.csv.processor.service;

import com.gameaccount.w2g.csv.processor.model.BetInfo;
import com.gameaccount.w2g.csv.processor.model.EventInfo;
import com.gameaccount.w2g.csv.processor.repo.BetInfoRepositoryRedisImpl;
import com.gameaccount.w2g.csv.processor.util.W2GUtils;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.CSVWriter;
import com.opencsv.exceptions.CsvValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@Service
public class CSVProcessor
{
    private static final Logger LOGGER = LoggerFactory.getLogger(CSVProcessor.class);
    private static final BufferedReader READER = new BufferedReader( new InputStreamReader( System.in ) );

    private static final String FILE_FOR_DB = "data.txt";
    private static final String FILE_TAXABLE_EVENTS = "taxable_events.csv";
    private static final String FILE_NOTIFIABLE_EVENTS = "notifiable_events.csv";

    private Path filePath = null;

    private final W2GProcessor w2GProcessor;
    private final RepositoryService repositoryService;

    @Autowired
    public CSVProcessor( final W2GProcessor w2GProcessor,
                         final RepositoryService repositoryService )
    {
        this.w2GProcessor = w2GProcessor;
        this.repositoryService = repositoryService;
    }

    public void initiate( )
    {
        afterInit( );
    }

    private void afterInit( )
    {
        LOGGER.info( "Please choose an option:" );
        LOGGER.info( "1: Parse .csv file." );
        LOGGER.info( "2: Process results from db." );
        try
        {
            final String userInput = READER.readLine();
            switch ( userInput )
            {
                case "1" : parseFileFromInput( ); break;
                case "2" : processW2GEvents(); break;
                default: LOGGER.info( "Option not supported. Shutting down.." ); System.exit( 0 );
            }
        }
        catch ( IOException e )
        {
            LOGGER.warn( e.getMessage(),
                         e );
        }

        // Log the results
        logResults( );

        // Save the results to a csv file
        saveResultsToFile();
    }

    /**
     *  Parse the file and create a new file
     *  which can be used to insert all the entries
     *  in redis db.
     *  End result will be a file called "data.txt" which
     *  holds all player's Id's as the Key and all their
     *  bets as the value (on individual lines).
     *  This will later be pipelined and inserted in redis
     *  so it can be processed.
     */
    private void parseFileFromInput( )
    {
        Reader reader = null;
        LOGGER.info( "Enter the absolute path to the .csv file: " );

        try
        {
            final String input = READER.readLine();

            filePath = Paths.get( input );
            reader = Files.newBufferedReader( filePath );

            parseCsvFile( reader );
        }
        catch ( IOException | SecurityException | NullPointerException e )
        {
            LOGGER.info( "Invalid file path specified or you do not have the required permissions. Please try again." );
        }
        finally
        {
            if (reader != null)
            {
                try
                {
                    reader.close();
                }
                catch ( IOException e )
                {
                    LOGGER.warn( e.getMessage(), e );
                }
            }
        }

    }

    /**
     *  Begin processing the entries from the db
     *  and calculate all w2g events.
     */
    private void processW2GEvents( )
    {
        w2GProcessor.processAllTaxEvents( );
    }

    private void parseCsvFile( final Reader reader )
    {
        LOGGER.info( "Processing file {}", filePath );

        try
        {
//            CSVParser parser = new CSVParserBuilder()
//                .withSeparator('|')
//                .build();

            CSVReader csvReader = new CSVReaderBuilder( reader )
//                .withCSVParser( parser )
                .build();

            parseCsvFileByLine( csvReader );
        }
        catch ( Exception e )
        {
            LOGGER.error( e.getMessage(), e );
        }

        finally
        {
            LOGGER.info( "Finished processing file: {}",
                         filePath );
        }
    }

    private void parseCsvFileByLine( final CSVReader csvReader )
        throws
        IOException,
        CsvValidationException
    {
        boolean firstLine = true;

        try ( Writer fileWriter = new OutputStreamWriter( new FileOutputStream( FILE_FOR_DB ) ) )
        {
            int recordsInFile = 0;
            String[] line;
            while ( ( line = csvReader.readNext( ) ) != null )
            {
                // check first line for header
                if ( firstLine )
                {
                    firstLine = false;

                    if ( W2GUtils.isHeader( line[ 0 ] ) )
                    {
                        continue;
                    }
                }

                BetInfo betInfo = createBetInfoFromLine( line );

                writeToFile( betInfo,
                             fileWriter );

                recordsInFile++;

                if ( recordsInFile % 100_000 == 0 )
                {
                    LOGGER.info( "Added {} entries to file {}",
                                 recordsInFile,
                                 FILE_FOR_DB);
                }

            }

            LOGGER.info( "File {} is ready.",
                         FILE_FOR_DB);
        }

    }

    private BetInfo createBetInfoFromLine( final String[] line )
    {
        return new BetInfo.Builder( )
                        .withCouponId( line[5].trim() )
                        .withCombinationId( line[6].trim() )
                        .withOutcomeId( line[7].trim() )
                        .withStake( new BigDecimal( line[12].trim() ) )
                        .withWin( new BigDecimal( line[13].trim() ) )
                        .withCustomerId( line[10].trim() )
                        .withStatusId( line[8].trim() )
                        .build( );
    }

    private void logResults( )
    {
        repositoryService.showContent( true );
    }

    /**
     * Saves the results in two files:
     * taxable_events.csv
     * notifiable_events.csv
     */
    public void saveResultsToFile( )
    {
        final List< EventInfo > taxableEvents = BetInfoRepositoryRedisImpl.getTaxableEvents();
        final List< EventInfo > notifiableEvents = BetInfoRepositoryRedisImpl.getNotifiableEvents();

        final String[] header = {"playerId","betId","outcomeId","totalBetAmount","totalWin","quotient","taxableAmount","federalTaxAmount"};

        CSVWriter csvWriter;
        try
        {
            csvWriter = new CSVWriter( new OutputStreamWriter( new FileOutputStream( FILE_TAXABLE_EVENTS ) ) );
            csvWriter.writeNext( header, false );

            for ( final EventInfo eventInfo : taxableEvents )
            {
                final String[] eventInfoLine = eventInfo.toString().split( "," );
                csvWriter.writeNext( eventInfoLine, false );
            }

            csvWriter.close();

            csvWriter = new CSVWriter( new OutputStreamWriter( new FileOutputStream( FILE_NOTIFIABLE_EVENTS ) ) );
            csvWriter.writeNext( header, false );

            for ( final EventInfo eventInfo : notifiableEvents )
            {
                final String[] eventInfoLine = eventInfo.toString().split( "," );
                csvWriter.writeNext( eventInfoLine, false );
            }

            LOGGER.debug( "Created files {} and {}",
                          FILE_TAXABLE_EVENTS,
                          FILE_NOTIFIABLE_EVENTS );
            csvWriter.close();
        }
        catch ( IOException e )
        {
            LOGGER.warn( e.getMessage(), e );
        }
    }

    private void writeToFile( final BetInfo betInfo, Writer fileWriter )
    {
        final StringBuilder stringBuilder = new StringBuilder(  );
        stringBuilder.append( "APPEND " )
            // The key
            .append( betInfo.getCustomerId() )
            .append( " " )
            // All values separated by "\\|"
            .append( betInfo.getCombinationId() )
            .append( "|" )
            .append( betInfo.getCouponId() )
            .append( "|" )
            .append( betInfo.getOutcomeId() )
            .append( "|" )
            .append( betInfo.getStatusId() )
            .append( "|" )
            .append( betInfo.getStake() )
            .append( "|" )
            .append( betInfo.getWin() )
            // Final separator for each bet
            .append( "#" );

        try
        {
            fileWriter.write( stringBuilder.toString() );
            fileWriter.write( System.getProperty( "line.separator" ) );
            fileWriter.flush();
        }
        catch ( IOException e )
        {
            LOGGER.warn( e.getMessage(),
                         e );
        }
    }

}
