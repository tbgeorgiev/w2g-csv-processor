package com.gameaccount.w2g.csv.processor.model;

import java.util.List;

public class Player
{
    private final String playerId;
    private final List< BetInfo > bets;

    private Player( final Builder builder )
    {
        this.playerId = builder.playerId;
        this.bets = builder.bets;
    }

    public String getPlayerId( )
    {
        return playerId;
    }

    public List< BetInfo > getAllBets( )
    {
        return bets;
    }

    public void addBet( final BetInfo bet )
    {
        bets.add( bet );
    }

    public static final class Builder
    {
        private String playerId;
        private List<BetInfo> bets;

        public Builder withPlayerId( final String playerId )
        {
            this.playerId = playerId;
            return this;
        }

        public Builder withBets( final List<BetInfo> bets )
        {
            this.bets = bets;
            return this;
        }

        public Player build( )
        {
            return new Player( this );
        }
    }
}
