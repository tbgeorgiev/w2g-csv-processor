package com.gameaccount.w2g.csv.processor.service;

import com.gameaccount.w2g.csv.processor.model.EventInfo;
import com.gameaccount.w2g.csv.processor.repo.BetInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class RepositoryService
{
    private final BetInfoRepository betInfoRepository;

    @Autowired
    public RepositoryService( @Qualifier ("redisImpl") final BetInfoRepository betInfoRepository )
    {
        this.betInfoRepository = betInfoRepository;
    }

    public void addTaxableEvent( final String dbKey,
                                 final EventInfo eventInfo )
    {
        betInfoRepository.addTaxableEvent( dbKey,
                                           eventInfo );
    }

    public void addNotifiableEvent( final String dbKey,
                                    final EventInfo eventInfo )
    {
        betInfoRepository.addNotifiableEvent( dbKey,
                                              eventInfo );
    }

    public Set<String> getAllPlayerKeys( )
    {
        return betInfoRepository.getAllPlayerKeys();
    }

    public String getAllBetsForPlayer( final String dbKey )
    {
        return betInfoRepository.getAllBetsForPlayer( dbKey );
    }

    public void showContent( final boolean detailedLog )
    {
        betInfoRepository.showContent( detailedLog );
    }

}
