package com.gameaccount.w2g.csv.processor.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import redis.clients.jedis.Jedis;

/**
 * Default config for connecting to redis.
 * Uses the default port 6379 and a timeout of 20000.
 */

@Configuration
public class RedisConfig
{
    @Bean
    public Jedis getJedis( )
    {
        return new Jedis( "localhost", 6379, 20000 );
    }
}
