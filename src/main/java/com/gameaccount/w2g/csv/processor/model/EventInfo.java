package com.gameaccount.w2g.csv.processor.model;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

public class EventInfo
{
    private final String playerId;
    private final List< String > betIdList;
    private final Set< String > outcomeIdList;
    private final BigDecimal totalBetAmount;
    private final BigDecimal totalWin;
    private final BigDecimal quotient;
    private final BigDecimal taxableAmount;
    private final BigDecimal federalTaxAmount;


    private EventInfo( final Builder builder )
    {
        this.playerId = builder.playerId;
        this.betIdList = builder.betIdList;
        this.outcomeIdList = builder.outcomeIdList;
        this.totalBetAmount = builder.totalBetAmount;
        this.totalWin = builder.totalWin;
        this.quotient = builder.quotient;
        this.taxableAmount = builder.taxableAmount;
        this.federalTaxAmount = builder.federalTaxAmount;
    }

    @Override
    public String toString( )
    {
        final StringBuilder stringBuilder = new StringBuilder( );
        stringBuilder.append( playerId )
            .append( "," )
            .append( betIdList )
            .append( "," )
            .append( outcomeIdList )
            .append( "," )
            .append( totalBetAmount )
            .append( "," )
            .append( totalWin )
            .append( "," )
            .append( quotient )
            .append( "," )
            .append( taxableAmount )
            .append( "," )
            .append( federalTaxAmount );

        return stringBuilder.toString();
    }

    public static final class Builder
    {
        private String playerId;
        private List< String > betIdList;
        private Set< String > outcomeIdList;
        private BigDecimal totalBetAmount;
        private BigDecimal totalWin;
        private BigDecimal quotient;
        private BigDecimal taxableAmount;
        private BigDecimal federalTaxAmount;

        public Builder withPlayerId( final String playerId )
        {
            this.playerId = playerId;
            return this;
        }

        public Builder withBetIdList( final List< String > betIdList )
        {
            this.betIdList = betIdList;
            return this;
        }

        public Builder withOutcomeIdList( final Set< String > outcomeIdList )
        {
            this.outcomeIdList = outcomeIdList;
            return this;
        }

        public Builder withTotalBetAmount( final BigDecimal totalBetAmount )
        {
            this.totalBetAmount = totalBetAmount;
            return this;
        }

        public Builder withTotalWin( final BigDecimal totalWin )
        {
            this.totalWin = totalWin;
            return this;
        }

        public Builder withQuotient( final BigDecimal quotient )
        {
            this.quotient = quotient;
            return this;
        }

        public Builder withTaxableAmount( final BigDecimal taxableAmount )
        {
            this.taxableAmount = taxableAmount;
            return this;
        }

        public Builder withFederalTaxAmount( final BigDecimal federalTaxAmount )
        {
            this.federalTaxAmount = federalTaxAmount;
            return this;
        }

        public EventInfo build( )
        {
            return new EventInfo( this );
        }
    }
}
